"use strict";
// alert("Hello World");

// console.log("Hello World");

// [Section] Syntax, Statements and Comments

    // Statements in programming are instructions that we tell the computer to perform.
    // JS statements usually end with semicolon(;).
    // Semicolons are not required in JS, but we will use it to help us train to locate where a statements end.

    // A syntax in programming, it is the set of rules that we describe how statements must be constructed.
    // All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in certain manner.

    // Comments are parts of the code that gets ignored by the language.
    // Comments are meant to describe the written code.

    /* 
        There are two types of comments:
        1. The single-line comment denoted by two slashes (//).
        2. The multi-line comment denoted by slash and asterisk.
    */

    // Variables are used to contain data.
    // Any information that is used by an application is stored in what we call the "memory".
    // When we create variables, certain portions of the device's memory is

    // This makes it easier for us to associate information store in our devices to actual "names" about information.

    // Declaring Variables - it tellsour devices that a variable name is created and is ready to store data.
    // Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined.
    // Syntax: let / const variableName;
    let myVariable = "Ada Lovelace";
    // const myVariable = "Ada Lovelace";

    // console.log() is useful for printing values of variables or certain results of codes into the Google Chrome browser's console.
    // Constants use of this thoughtout developing an application will save us time and build good habit in always checking the output of our code.
    console.log(myVariable);

    /*
        Guides in writing variables:
            1. Use the let keyword followed by the variable name of your choice
            and use the assignment operator (=) to assign a value.
            2. Variable names should start with lowercase character, use camelCase for multiple words.
            3. For constant variables, use the 'const' keyword.
                using let: we can change the value of the variable.
                using const: we cannot change the value of the variable.
            4. Variable names should be indicative (descriptive) of the value being store to avoid confusion.
    */

    // Declare and initialize variables
    // Initializing variables - the instance when a variable is given it's initial/starting value.
    // Syntax: let / const variableName = value;
    let productName = "destop computer";
    console.log(productName);

    let productPrice = 189999;
    console.log(productPrice);

    // In the context of certain application, some variables / information are constant and should not be changed.
    //  In this example, the interest rate for a loan, saving account or a mortgage must not be change due to real world concerns.

    const interest = 3.539;
    console.log(interest);

    // Reassigning a variable means changing it's initial pr previous valueinto another value.
    // Syntax: variableName = new value;
    productName = "laptop";
    console.log(productName);

    // let variable cannot be re-declared within its scope.

    // Values of constant cannot be changed and will simply return an error.
    /* interest = 4.489;
    console.log(interest); */

    // Reassigning variables and initializing variables.
    let supplier;               //Declaration
    supplier = "Zuitt Store";   //Initialization
    supplier = "Zuitt Merch";   //Reassignment

    // var vs let/const
    // var is also used in declaring variable but var is an EcmaScript1 feature [ES1 (Javascript 1997)].
    // let/const was introduced as a new feature in ES6 (2016).

    // What makes let/const different var?
    a = 5;
    console.log(a);
    var a;

    /* b = 6;
    console.log(b);
    let b; */

    // let/const local/global scope
    // Scope essentially means where these variables are available for use
    // let and const are block scope
    // A block is a chuck of code bounded by {}. Anything within the curlky braces is a block.

    let outerVariable = "hello from the other side";

    {
        let innerVariable = "Hello from the block";
        console.log(innerVariable);
        console.log(outerVariable);
    }

    {
        let innerVariable = "Hello from the second block";
        console.log(innerVariable);
        let outerVariable = "hello from the side";
        console.log(outerVariable);
    }

    console.log(outerVariable);
    // console.log(innerVariable);

    // Multiple variable declarations
    // Multiple variables can be declared in one line.
    let productCode = "DC017", productBrand = "Dell";
    console.log(productCode, productBrand);
    console.log(productCode + productBrand);

    // Using a variable with a reserved keyword
    /* const let = "Hello";
    console.log(let); */

// [Section] Data Types

    // Strings - are series of characters that create a word, a phrase, a sentence or anything related to creating text.
    // String in JavaScript can be written using either single (') and double () quotes.
    // In other programming languages, only the double quotes can be used for creating string.

    let country = "Philippines";
    let province = 'Metro Manila';

    // Concatenate
    // Multiple string value can be combined to create a single string using the "+" symbol

    let fullAddress = province + ", " + country;
    console.log(fullAddress);

    let greeting = "I live in the " + country;
    console.log(greeting);

    // The escaoe characters (\) in strings in combination with other characters can produce different effects.

    let mailAddress = "Metro Manila\nPhilippines";
    console.log(mailAddress);

    let message = 'John\'s employees went home early';
    console.log(message)

    // Numbers
    // Integers / Whole Numbers
    let count = "26";
    let headcount = 26;
    console.log(count);
    console.log(headcount);

    // Decimeal Numbers / Fractions
    let grade = 98.7;
    console.log(grade);

    // Exponential Notation
    let planetDistance = 2e10;
    console.log(planetDistance);

    console.log("John's grade last quarter is " + grade);
    console.log(count + headcount);

    // Boolean
    // Boolean values are normally used to store values relating to the state of certain things.

    let isMarried = false;
    let isGoodConduct = true;
    console.log(isMarried);
    console.log(isGoodConduct);

    console.log("isMarried: " + isMarried);

    // Arrays
    // Arrays are a special kind of data type that's used to store multiple values.
    // Arrays can store different data stype but is normally use to store similar data types.
    // Syntax: let/const arrayName = [elementA, elementB, elementC . . .]

    let grades = [98.7, 92.1, 90.2, 94.6];
    console.log(grades[1]);
    console.log(grades);
    
    // Different data types
    // Storing different data types inside an array is not recommened because it will not make sense in the context of programming.
    let details = ["John", "Smith", 32, true];
    console.log(details);

    // Objects are another special kind of data type that's used to mimic real world objects/items.
    // They're used to create a complex data that contains pieces of information that are relevant to each other
    /* 
    Syntax: 
        let/const objectName {
            propertyA: value,
            propertyB: value
        } 
    */

    let person = {
        fullname: "Juan Dela Cruz",
        age: 35,
        isMarried: false,
        contact: ["09171234567", "8123 4567"],
        address: {
            houseNumber: "345",
            city: "Manila"
        }
    }

    console.log(person);
    console.log(person.age);
    console.log(person.contact[1]);
    console.log(person.address.city);

    // typeof operator is ised to determine the type of data or the value of a variable.
    console.log(typeof person);
    // Note: Array is special type of object with methods and functions to manipulate it.
    console.log(typeof details);


    /*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

    */

    const anime = ["one piece", "one punch man", "attack on titans"];
    console.log(anime);
    // anime = ["kimetsu no yaiba"];
    anime[0] = "kimetsu no yaiba";
    console.log(anime);
    console.log(anime[5] = "dxd");
    console.log(anime);

    // Null is used to intentionally to express the absence of a value in a variable/initialization
    // Null simply means that a data type was assigned to a variable bit it does not hold any value/amount or is nullified.
    let spouse = null;
    console.log(spouse);

    // Undefined represents the state of a variable that has been declared but without value.
    let fullName;
    console.log(fullName);
    
    // Undefined vs Null
    // The difference between undefined and null is that for undefined, a variable was created but not provided a value.
    // null means that a variable was created and was assign a value that does not hold any value/amount.